import Vue from 'vue'

/**
 * 模块化
 */
Vue.prototype.ImConfig = {
	showTabbar_discover: true, //是否显示 下方菜单栏的 发现功能
	chatPage: { // 首页
		showAddBtn: true ,//add按钮是否展示
	},
	contactsPage: { //通讯录页

	},
	discoverPage: { //发现页
		show: true,//是否显示发现页
	},
	mePage: { //我 页

	}
}
