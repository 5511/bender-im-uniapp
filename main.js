import Vue from 'vue'
import App from './App'
import tabBar from './common/tabbar/tabbar.vue'  
import store from './sdk/store/index.js'
import BenderSdk from './sdk/sdk.js'
import httpInterceptor from './sdk/config/request.config.js'
import ImHttp from './sdk/utils/request.js'
import ImConfig from './config/index.js'
import MsgUtil from './sdk/utils/msgUtil.js'
Vue.component('tab-bar', tabBar)

Vue.prototype.$store = store
Vue.prototype.$ImHttp= ImHttp

Vue.config.productionTip = false

App.mpType = 'app'

const app = new Vue({
    ...App,
	ImConfig,
	BenderSdk,
	MsgUtil,
	ImHttp,
	store
})
Vue.use(httpInterceptor, app)
app.$mount()
