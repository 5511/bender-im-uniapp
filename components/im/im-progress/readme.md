
value:进度条百分比值0~100

disabled:是否禁止拖动进度

activeColor:进度条颜色（也可以直接重写样式）

min: 最小值

max: 最大值

step:步长，取值必须大于 0整数

line-size: Number，轨道的高度，默认4，单位rpx

block-size: Number，滑块的大小，默认32，单位rpx

blockColor: String，圆圈内圈颜色，默认值'#00e3ab'

blockOuterColor: String，圆圈外圈颜色，默认值'rgba(0, 227, 171, 0.2)'

2. 方法
changing 拖动过程中触发的事件(包含点击)

changed 拖动完成触发的事件（点击跳转完成也算）

-回调值 e{ progress: '进度条进度（0 ~ 100）百分比', value: '进度对应的值'}

可以自己二次扩展

