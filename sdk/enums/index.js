const ImEnum = {}

ImEnum.DataKeyEnum = {// 全局事件key 不可更改
	CHAT_LIST:'imChatListDataKey',//增加会话框会话 必须为list
	addMsgDataKey:'addMsgDataKey',//增加消息对话框的消息
	contactsDataKey:'imUserListDataKey',//通讯录
	initSHQMsgListDataKey:'initSHQMsgListDataKey',//通讯录
}


ImEnum.ImStorageEnum = {// 本地缓存的key
	chatKey: 'im_chatList_key',
	msgKey:'im_msg_key',
	loginUserInfo:'im_loginUserInfo_key',
	loginNameAndPaw:'im_loginNameAndPaw_key',
	loginToken:'im_loginToken_key',
	contactsList:'im_user_contacts_key',
	SHQMsgList:'im_SHQMsgList_key'


}
ImEnum.ImMsgTypeEnum = {//消息类型-不可修改
	text:'text',
	img:'img',
	video:'video',
	audio:'audio',
	tag:'tag'
}


ImEnum.OtherEnum = {// 数据的 枚举
	USER_TYPE: {//用户类型
		USER:2,
		GROUP:1,
		CHAT_ROOM:0
	},
	FRIEND_TYPE:{ //好友类型
		UN_KONW:0,
		FRIEND:1,
		BLACK_LIST:2,
	}
	
}



export default ImEnum
