import ImEnum from '../../enums/index.js'
import TransUtil from '../../config/transform-util/index.js'
import ImStg from '../../utils/ImStorage.js'
import Vue from "vue";
const wordArr = [
	'A',
	'B',
	'C',
	'D',
	'E',
	'F',
	'G',
	'H',
	'I',
	'J',
	'K',
	'L',
	'M',
	'N',
	'O',
	'P',
	'Q',
	'R',
	'S',
	'T',
	'U',
	'V',
	'W',
	'X',
	'Y',
	'Z',
	'#'
]
const ImUser = {
	state: {
		userListState: 0, //更新 通讯录数据
	},
	mutations: {
		imUserListDataKey(state, obj) {
			Vue.set(state, 'chatListState', state.chatListState++)
		},

	},
	actions: {
		/**
		 * @param {Object} payload
		 * @param {Object}  obj 为List 数组
		 */
		imUserListDataKey: function(payload, obj) {
			if (obj) {
				/**
				 * 装换 后台dto 适配前端dto
				 */
				if (obj) {
					obj = TransUtil.setContactsDto(obj)
				}
				let map = ImStg.getImStorageSync(ImEnum.ImStorageEnum.contactsList)
				if (map) {
					map[obj.id] = obj
				} else {
					map = obj
				}
				ImStg.setImStorageSync(ImEnum.ImStorageEnum.contactsList, map)
				payload.commit("imUserListDataKey", obj);
			}

		},
	},
	getters: {
		imUserSortListGetter(state) { //获取会话列表
			let temp = state.userListState //这个是用于监听 会话列表是否变更了的，不能删除
			let userList = ImStg.getImStorageSync(ImEnum.ImStorageEnum.contactsList)
			let resultList = new Array()
			if (userList) {
				wordArr.forEach(arr => {
					let tempList = new Array()
					userList.forEach(user => {
						if (user.letter === arr) {
							tempList.push(user)
						}
					})
					if (tempList.length > 0) {
						let obj = {
							letter: arr,
							data: tempList
						}
						resultList.push(obj)
					}

				})
			}
			return resultList
		},
		imFriendCountGetter(state) { //好友数量
			let temp = state.userListState //这个是用于监听 会话列表是否变更了的，不能删除
			let userList = ImStg.getImStorageSync(ImEnum.ImStorageEnum.contactsList)
			let count = 0
			if (userList.length > 0) {
				count = userList.length
			}
			return count
		}
	}
}
export default ImUser
