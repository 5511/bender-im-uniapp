import ImEnum from '../../enums/index.js'
import TransUtil from '../../config/transform-util/index.js'
import ImStg from '../../utils/ImStorage.js'
import Vue from "vue";
const ImSHqun = {
	state: {
		SHQMsgListState: [],//当前人的生活区信息
		OtherSHQMsgList: []
	},
	mutations: {
		setSHQMsgList(state, msgList) {
			Vue.set(state,'SHQMsgListState',state.SHQMsgListState++)
		},
	},
	actions: {
		initSHQMsgListDataKey: function(payload,obj) {
			var temp  = TransUtil.setSHQMsgListDto(obj)
			ImStg.setImStorageSync(ImEnum.ImStorageEnum.SHQMsgList,temp)
			payload.commit("setSHQMsgList", obj);
		},


	},
	getters: {
		sHQMsgListGetter(state){//获取
			let temp = state.SHQMsgListState
			let msgList = ImStg.getImStorageSync(ImEnum.ImStorageEnum.SHQMsgList)
			if(!msgList){
				msgList =[]
			}
			return msgList
		},

	}
}
export default ImSHqun
