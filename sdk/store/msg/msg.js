import ImEnum from '../../enums/index.js'
import TransUtil from '../../config/transform-util/index.js'
import ImStg from '../../utils/ImStorage.js'
import Vue from "vue";
const ImMsg = {
	state: {
		msgListState: 0,//更新会话列表的 状态 这个状态变更了 说明会话列表 也更新了
		
	},
	mutations: {
		addMsgDataKey(state, obj) {
			Vue.set(state,'msgListState',state.msgListState++)
		},

	},
	actions: {
		/**
		 * @param {Object} payload
		 * @param {Object} obj 为List 数组
		 */
		addMsgDataKey: function(payload,obj) {		
			/**
			 * 装换 后台dto 适配前端dto
			 */
			
			if(obj&&obj.msg){
				var temp  = TransUtil.setMsgDto(obj.msg)
				obj.msg = temp
				let msgMap = ImStg.getImStorageSync(ImEnum.ImStorageEnum.msgKey)
				if(msgMap){
					if(msgMap[obj.inChatUserId]&&msgMap[obj.inChatUserId].length>0){
						let tempMap = msgMap[obj.inChatUserId]
						obj.msg.forEach(data =>{
							tempMap.push(data)
						})
						msgMap[obj.inChatUserId] = tempMap
					}else{
						msgMap[obj.inChatUserId]= obj.msg
					}
				}else{
					msgMap = new Map()
					console.log("obj.inChatUserId--"+obj.inChatUserId)
					console.log("obj.msg--"+JSON.stringify(obj.msg))
					
					msgMap[obj.inChatUserId ]= obj.msg
				}
				ImStg.setImStorageSync(ImEnum.ImStorageEnum.msgKey,msgMap)
				payload.commit("addMsgDataKey", obj);
			}
		},
	},
	getters: {
		msgListGetter(state){//获取
			let temp = state.msgListState//这个是用于监听 会话列表是否变更了的，不能删除
			let msgList = ImStg.getImStorageSync(ImEnum.ImStorageEnum.msgKey)
			if(!msgList){
				msgList =[]
			}
			return msgList
		},
	}
}
export default ImMsg
