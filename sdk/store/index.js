import Vue from "vue";
import Vuex from "vuex"
import ImChat from "./chat/chat.js"
import ImMsg from './msg/msg.js'
import ImUsers from './user/index.js'
import ImSHqun from './SHqun/index.js'


Vue.use(Vuex)

const store = new Vuex.Store({
	modules: {
		ImChat: ImChat,
		ImMsg: ImMsg,
		ImUsers: ImUsers,
		ImSHqun: ImSHqun
	}
})

export default store
