import ImEnum from '../../enums/index.js'
import TransUtil from '../../config/transform-util/index.js'
import ImStg from '../../utils/ImStorage.js'
import Vue from "vue";
const ImChat = {
	state: {
		chatListState: 0, //更新会话列表的 状态 这个状态变更了 说明会话列表 也更新了

	},
	mutations: {
		imChatListDataKey(state, obj) {
			Vue.set(state, 'chatListState', state.chatListState++)
		},

	},
	actions: {
		/**
		 * @param {Object} payload
		 * @param {Object}  obj 为List 数组
		 */
		imChatListDataKey: function(payload, obj) {
			/**
			 * 装换 后台dto 适配前端dto
			 */
			if (obj) {
				obj = TransUtil.setChatInfoDto(obj)
			}
			let chatList = ImStg.getImStorageSync(ImEnum.ImStorageEnum.chatKey)
			if (chatList) {
				let temp = obj
				chatList.forEach(data => {
					let falg = true
					obj.forEach(obj1 => {
						if (obj1.id === data.id) {
							falg = false
						}
					})
					if (falg) {
						temp.push(data)
					}
				})
				chatList = temp
			} else {
				chatList = obj
			}
			ImStg.setImStorageSync(ImEnum.ImStorageEnum.chatKey, chatList)
			payload.commit("imChatListDataKey", obj);
		},
		removeChat: function(payload, obj) {
			let chatList = ImStg.getImStorageSync(ImEnum.ImStorageEnum.chatKey)
			chatList.splice(obj, 1)
			ImStg.setImStorageSync(ImEnum.ImStorageEnum.chatKey, chatList)
			payload.commit("imChatListDataKey", obj);
		}
	},
	getters: {
		chatListGetter(state) { //获取会话列表
			let temp = state.chatListState //这个是用于监听 会话列表是否变更了的，不能删除
			let chatList = ImStg.getImStorageSync(ImEnum.ImStorageEnum.chatKey)
			if (!chatList) {
				chatList = []
			}
			return chatList
		},

	}
}
export default ImChat
