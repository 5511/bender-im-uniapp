/**
 * 模块化
 */
const ImStorage = {}

ImStorage.getImStorageSync = function(key) {
	const value = uni.getStorageSync(key);
	console.log('getImStorageSync');
	if (value) {
		return JSON.parse(value)
	} else {
		return ''
	}
}
ImStorage.setImStorageSync = function(key, obj) {
	if (obj) {
		uni.setStorageSync(key, JSON.stringify(obj));
		console.log('setStorage-Success');
	} else {
		uni.setStorageSync(key, "");
	}
}

ImStorage.clearImStorageSync = function(key, obj) {
	uni.clearStorageSync();
}



ImStorage.getImStorage = function(key, obj) {
	uni.getStorageInfo({
		success: function(res) {
			console.log('getImStorage');
			if (res) {
				return JSON.parse(res)
			} else {
				return ''
			}
		}
	});
}



ImStorage.setImStorage = function(key, obj) {
	if (obj) {
		uni.setStorage({
			key: key,
			data: JSON.stringify(obj),
			success: function() {
				console.log('setStorage-Success');
			}
		});
	} else {
		uni.setStorage({
			key: key,
			data: "",
			success: function() {
				console.log('setStorage-Success');
			}
		});
	}

}


ImStorage.clearImStorage = function(key, obj) {
	uni.clearStorage();
}
export default ImStorage
