const MsgUtil = {}


MsgUtil.showConfirmModal = function(title, showConfirm, confirmText, confirmUrl, showCancel, cancelText, cancelUrl) {
	uni.showModal({
		title: title ? title : "无信息",
		confirmText: confirmText ? confirmText : "确定",
		confirmColor: "green",
		showCancel: showCancel,
		cancelText: cancelText ? cancelText : "取消",
		success: function(res) {
			if (res.confirm) {
				if (confirmUrl && confirmUrl != '') {
					uni.redirectTo({
						url: confirmUrl
					});
				}
			}
		},
		fail: function() {
			if (cancelUrl && cancelUrl != '') {
				uni.redirectTo({
					url: cancelUrl
				});
			}

		}
	});
}

MsgUtil.showModal = function(title) {
	uni.showModal({
		title: title ? title : "无信息",
		confirmText: "确定",
		confirmColor: "green",
		showCancel: false,
		success: function(res) {
		},
	});
}

export default MsgUtil
