
const TransUtil = {}

/**
  登录请求
 * @param {Object} 
 * {
		userName:userName,
		pwd :pwd
	}
 */
TransUtil.setloginReq = function(obj) {
		return obj
}
/** 
 * 登录回参请求
 * @param {Object} obj
 */
TransUtil.setloginRes = function(obj) {
		return obj
}



/**
 * 
 * 会话列表 
 * @param {Object} obj
 * [{
		id:1,
		name:'波多野溪子',
		nickName:'波多野溪子',
		avatar:'https://cdn.quasar.dev/img/avatar.png',
		type:ImEnum.OtherEnum.USER_TYPE.USER,
		unReadCount :1,
		lastChatTime:'10:22',
		lastChatContent:"你好啊，傻逼",
	}]
 */
TransUtil.setChatInfoDto = function(obj) {
		/**
		 * 此处用于替换 后台返回的dto字段 与我的不一致时 处理的地方，把你的dto转成我页面要的dto
		 */
		return obj
}

/**
 * @param {Object} obj
 * 
 * 
 * {
			msgId:1,
			sender:1,
			sendYear:'2020',
			sendDay:'07-02',
			sendTime:"10:45",
			msgType:ImEnum.ImMsgTypeEnum.text,
			count:21,//音频的长度
			msgStr:'里面有2个域名的申请和Niginx的配置',
			url:'https://cdn.quasar.dev/img/mountains.jpg'
		},
		
		// ImEnum.ImMsgTypeEnum = {//消息类型-不可修改
		// 	text:'text',
		// 	img:'img',
		// 	video:'video',
		// 	audio:'audio',
		// 	tag:'tag'
		// }
 */
TransUtil.setMsgDto = function(obj) {
		/**
		 * 此处用于替换 后台返回的dto字段 与我的不一致时 处理的地方，把你的dto转成我页面要的dto
		 */
		return obj
}

/**
 * @param {Object} obj
 *  登录人 信息
 * 
 */
TransUtil.setLoginDto = function(obj) {
		/**
		 * 此处用于替换 后台返回的dto字段 与我的不一致时 处理的地方，把你的dto转成我页面要的dto
		 */
		return obj
}

/**
 * @param {Object} 
 * 好友列表
 */
TransUtil.setLoginFriendDto = function(obj) {
		/**
		 * 此处用于替换 后台返回的dto字段 与我的不一致时 处理的地方，把你的dto转成我页面要的dto
		 */
		return obj
}
	
	
/**
 * @param {Object} 
 *  通讯录 
 */
TransUtil.setContactsDto = function(obj) {
		/**
		 * 此处用于替换 后台返回的dto字段 与我的不一致时 处理的地方，把你的dto转成我页面要的dto
		 */
		return obj
}


/** 生活群
 * @param {Object} obj
 */
	
TransUtil.setSHQMsgListDto = function(obj) {
		/**
		 * 此处用于替换 后台返回的dto字段 与我的不一致时 处理的地方，把你的dto转成我页面要的dto
		 */
		return obj
}

	
	
	

export default TransUtil
