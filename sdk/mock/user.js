const ImUser = {}

/**
 * 当前登录人
 */
ImUser.loginUserInfo = {
	id: '1',
	name: 'Geez',
	accountName: 'zenghj1994',
	password: '123456',
	avatar: 'https://cdn.quasar.dev/img/avatar.png',
	phone: '12345678910',
	sex: '男',
	remark: '我变强了，也变秃了！'
}

/**
 * 好友列表
 */
ImUser.firendMap = [{
		id: '100',
		name: '波多野溪子',
		accountNum:'4545454',
		nickName: '波多野溪子',
		avatar: 'https://cdn.quasar.dev/img/avatar.png',
		phone: '12345678910',
		sex: '女',
		remark: '天大地大 ,我最大',
		letter: 'B'

	},
	{
		id: '2',
		name: '深川铃',
		nickName: '深川铃',
		accountNum:'4545454',
		avatar: 'https://cdn.quasar.dev/img/avatar1.jpg',
		phone: '12345678910',
		sex: '女',
		remark: '我变强了，也变秃了',
		letter: 'S'
	},
	{
		id: '3',
		name: '北野望',
		nickName: '北野望',
		accountNum:'4545454',
		avatar: 'https://cdn.quasar.dev/img/avatar2.jpg',
		phone: '12345678910',
		sex: '女',
		remark: '我变强了，也变秃了',
		letter: 'B'
	},
	{
		id: '4',
		name: '王野望',
		nickName: '北野望',
		accountNum:'4545454',
		avatar: 'https://cdn.quasar.dev/img/avatar3.jpg',
		phone: '12345678910',
		sex: '男',
		remark: '我变强了，也变秃了',
		letter: 'W'
	},
	{
		id: '5',
		name: '桥本凉',
		nickName: '桥本凉',
		accountNum:'4545454',
		avatar: 'https://cdn.quasar.dev/img/avatar4.jpg',
		phone: '12345678910',
		sex: '男',
		remark: '我变强了，也变秃了',
		letter: 'Q'
	},
	{
		id: '6',
		name: '冬月枫',
		nickName: '冬月枫',
		accountNum:'4545454',
		avatar: 'https://cdn.quasar.dev/img/avatar5.jpg',
		phone: '12345678910',
		sex: '男',
		remark: '我变强了，也变秃了',
		letter: 'D'
	},
	{
		id: '7',
		name: '铃村爱里',
		nickName: '铃村爱里',
		accountNum:'4545454',
		avatar: 'https://cdn.quasar.dev/img/avatar3.jpg',
		phone: '12345678910',
		sex: '男',
		remark: '我变强了，也变秃了',
		letter: 'L'
	},
	{
		id: '8',
		name: '森川凉花',
		nickName: '森川凉花',
		accountNum:'4545454',
		avatar: 'https://cdn.quasar.dev/img/avatar2.jpg',
		phone: '12345678910',
		sex: '男',
		remark: '我变强了，也变秃了',
		letter: 'S'
	},
	{
		id: '9',
		name: '清水健',
		nickName: '清水健',
		accountNum:'4545454',
		avatar: 'https://cdn.quasar.dev/img/avatar1.jpg',
		phone: '12345678910',
		sex: '男',
		remark: '我变强了，也变秃了',
		letter: 'Q'
	},
	{
		id: '10',
		name: '加藤鹰',
		nickName: '加藤鹰',
		accountNum:'4545454',
		avatar: 'https://cdn.quasar.dev/img/avatar5.jpg',
		phone: '12345678910',
		sex: '男',
		remark: '我变强了，也变秃了',
		letter: 'J'
	},
	{
		id: '11',
		name: '阿童木',
		nickName: '阿童木',
		accountNum:'4545454',
		avatar: 'https://cdn.quasar.dev/img/avatar5.jpg',
		phone: '12345678910',
		sex: '男',
		remark: '我变强了，也变秃了',
		letter: 'A'
	}
]

export default ImUser
