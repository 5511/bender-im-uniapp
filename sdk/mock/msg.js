import ImEnum from '../../sdk/enums/index.js'

const ImMsgList ={}


/**
 * 信息 JSON
 * {
			msgId:1,
			sender:1,
			sendYear:'2020',
			sendDay:'07-02',
			sendTime:"10:45",
			msgType:ImEnum.ImMsgTypeEnum.text,
			count:21,//视频或音频的长度
			msgStr:'里面有2个域名的申请和Niginx的配置',
			url:'https://cdn.quasar.dev/img/mountains.jpg'
		},
 * 
 */
ImMsgList.msgMap = {
	"group-100":[
		{
			msgId:1,
			sender:'1',
			sendYear:'2020',
			sendDay:'07-02',
			sendTime:"10:45",
			msgType:ImEnum.ImMsgTypeEnum.text,
			msgStr:'里面有2个域名的申请和Niginx的配置[:p]',
			url:'https://cdn.quasar.dev/img/mountains.jpg'
		},
		{
			msgId:2,
			sender:'2',
			sendYear:'2020',
			sendDay:'07-02',
			sendTime:"10:46",
			msgType:ImEnum.ImMsgTypeEnum.img,
			msgStr:'里面有2个域名的申请和Niginx的配置',
			url:'https://cdn.quasar.dev/img/mountains.jpg'
		},
		{
			msgId:3,
			sender:'1',
			sendYear:'2020',
			sendDay:'07-02',
			sendTime:"10:50",
			msgType:ImEnum.ImMsgTypeEnum.video,
			url:'https://img.cdn.aliyun.dcloud.net.cn/guide/uniapp/%E7%AC%AC1%E8%AE%B2%EF%BC%88uni-app%E4%BA%A7%E5%93%81%E4%BB%8B%E7%BB%8D%EF%BC%89-%20DCloud%E5%AE%98%E6%96%B9%E8%A7%86%E9%A2%91%E6%95%99%E7%A8%8B@20181126-lite.m4v'
		},
		{
			msgId:4,
			sender:'2',
			sendYear:'2020',
			sendDay:'07-02',
			sendTime:"10:50",
			msgType:ImEnum.ImMsgTypeEnum.audio,
			audioText:'录音',
			count:21,
			url:'https://img-cdn-qiniu.dcloud.net.cn/uniapp/audio/music.mp3'
		},
		{
			msgId:5,
			sender:'3',
			sendYear:'2020',
			sendDay:'07-02',
			sendTime:"10:50",
			msgType:ImEnum.ImMsgTypeEnum.text,
			msgStr:'[:p]有些表是存在的'
		},
		{
			msgId:6,
			sender:'3',
			sendYear:'2020',
			sendDay:'07-02',
			sendTime:"10:50",
			msgType:ImEnum.ImMsgTypeEnum.text,
			msgStr:'你给我的sql是重建表'
		},
		{
			msgId:7,
			sender:'1',
			sendYear:'2020',
			sendDay:'07-02',
			sendTime:"10:50",
			msgType:ImEnum.ImMsgTypeEnum.text,
			msgStr:'直接覆盖掉 就行 [:p]，覆盖前 麻烦国际惯例 备份一下'
		},
	]
	
}


export default ImMsgList

