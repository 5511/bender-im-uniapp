const ImSHqunMock ={}

ImSHqunMock.SHQMsgList=[
				{
					img: {
						url: 'https://cdn.quasar.dev/img/avatar.png'
					},
					title: {
						text: 'Geez'
					},
					location: {
						remark: '深圳.世界之窗'
					},
					time: '7小时前',
					content: '现实是此岸，成功是彼岸，中间隔着湍急的河流，兴趣便是河上的桥，只要行动就可以通过。',
					tag: {
						text: '1小时前'
					},
					imgs: [
						'https://cdn.quasar.dev/img/mountains.jpg',
						'https://cdn.quasar.dev/img/mountains.jpg',
						'https://cdn.quasar.dev/img/mountains.jpg',
						'https://cdn.quasar.dev/img/mountains.jpg',
						'https://cdn.quasar.dev/img/mountains.jpg',
						'https://cdn.quasar.dev/img/mountains.jpg',
						'https://cdn.quasar.dev/img/mountains.jpg',
						'https://cdn.quasar.dev/img/mountains.jpg',
						'https://cdn.quasar.dev/img/mountains.jpg'
					],
					header: {
						bgcolor: '#F7F7F7',
						line: true
					},
					zanList: [
						{
							id: 1,
							name: 'Geez'
						},
						{
							id: 2,
							name: '哈哈'
						},
						{
							id: 2,
							name: '哈哈'
						},
						{
							id: 2,
							name: '哈哈'
						}
					],
					pingLunList: [
						{
							id: 1,
							name: '哈哈',
							atUser: '',
							remark: '哇，拍的正好看啊'
						},
						{
							id: 1,
							name: 'Geez',
							atUser: '哈哈',
							remark: '谢谢你的好评，我会继续努力的！'
						}
					]
				},
				{
					img: {
						url: 'https://cdn.quasar.dev/img/avatar.png'
					},
					title: {
						text: 'Geez'
					},
					location: {
						remark: '深圳.世界之窗'
					},
					time: '7小时前',
					content: '现实是此岸，成功是彼岸，中间隔着湍急的河流，兴趣便是河上的桥，只要行动就可以通过。',
					tag: {
						text: '1小时前'
					},
					header: {
						bgcolor: '#F7F7F7',
						line: true
					},
					zanList: [
						{
							id: 1,
							name: 'Geez'
						},
						{
							id: 2,
							name: '哈哈'
						},
						{
							id: 2,
							name: '哈哈'
						},
						{
							id: 2,
							name: '哈哈'
						}
					],
					pingLunList: [
						{
							id: 1,
							name: '哈哈',
							atUser: '',
							remark: '哇，拍的正好看啊'
						},
						{
							id: 1,
							name: 'Geez',
							atUser: '哈哈',
							remark: '谢谢你的好评，我会继续努力的！'
						}
					]
				},
				{
					img: {
						url: 'https://cdn.quasar.dev/img/avatar.png'
					},
					title: {
						text: 'Geez'
					},
					location: {
						remark: '深圳.世界之窗'
					},
					time: '7小时前',
					content: '现实是此岸，成功是彼岸，中间隔着湍急的河流，兴趣便是河上的桥，只要行动就可以通过。',
					tag: {
						text: '1小时前'
					},
					imgs: ['https://cdn.quasar.dev/img/mountains.jpg'],
					header: {
						bgcolor: '#F7F7F7',
						line: true
					},
					zanList: [
						{
							id: 1,
							name: 'Geez'
						},
						{
							id: 2,
							name: '哈哈'
						},
						{
							id: 2,
							name: '哈哈'
						},
						{
							id: 2,
							name: '哈哈'
						}
					],
					pingLunList: [
						{
							id: 1,
							name: '哈哈',
							atUser: '',
							remark: '哇，拍的正好看啊'
						},
						{
							id: 1,
							name: 'Geez',
							atUser: '哈哈',
							remark: '谢谢你的好评，我会继续努力的！'
						}
					]
				}
			]
		

export default ImSHqunMock
