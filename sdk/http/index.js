import HttpUtils from '../utils/request.js'
const HttpIndex = {}

const LOGIN_URL = '/imUser/public/login.im'//登录的api
const REGISTER_URL = '/imUser/public/registerUser.im'//注册的api
const getUserInfoByKeyPage_URL = '/imUser/public/getUserInfoByKeyPage.im'//更具accountToken 和昵称 手机号 模糊查询用户


/**
 *  登录
 * @param {Object} userName 账号
 * @param {Object} pwd 密码
 */
HttpIndex.loginApi = async function(userName, pwd) {
	return await HttpUtils.post(LOGIN_URL + '?userToken=' + userName + '&pwd=' + pwd)
}

/**
 * 注册
 * @param {Object} registerUser
 */
HttpIndex.registerUser = async function(registerUser) {
	return await HttpUtils.post(REGISTER_URL, registerUser)
}

/**更具accountToken 和昵称 手机号 模糊查询用户
 * @param {Object} pageNum
 * {
	 pageNum:
	 pageSize:
	 searchKey:
 }
 */
HttpIndex.getUserInfoByKeyPage = async function(pageParam) {
	return await HttpUtils.post(getUserInfoByKeyPage_URL, pageParam)
}


export default HttpIndex
