import Vue from 'vue'
import ImEnum from './enums/index.js'
import store from './store/index.js'
import ImStorage from './utils/ImStorage.js'
/**
 * 基本配置
 */
import ImCommon from "./store/common/index.js"

/**
 * 聊天等api
 */
import BenderChatApi from './api/chat-api.js'

/**
 * 用户相关的api
 */
import BenderUserApi from './api/user-api.js'

/**
 * 数据及事件
 */
import chatStore from './store/chat/chat.js'

/**
 * 假数据mock
 */

import chatMock from './mock/chat.js'
import msgMock from './mock/msg.js'
import userMock from './mock/user.js'
import sHQunMock from './mock/SHQun.js'


import BenderIM from './api/BenderIM.js'

import HttpClinet from './http/index.js'

/**
 * 数据dto转换
 */
import TransUtil from './config/transform-util/index.js'


Vue.prototype.BenderSdk = {
	BenderHttpApi: HttpClinet,
	/**
	 * @param {Object} 
	 *  websocke是否连接成功
	 */
	loginSatet: BenderIM.socketOpen,

	/**
	 * 登录操作
	 * booMock  是否使用假数据 
	 */
	loginIm: function(booMock, token) {
		/**
		 * 初始化数据
		 */
		/**
		 * 判断是否使用假数据  
		 */

		if (booMock) {
			console.log('BenderSdk-----')
			/**
			 * 假数据要清缓存
			 */
			ImStorage.clearImStorageSync()
			/**
				初始化登录人
			 */
			let loginUserInfo = TransUtil.setLoginDto(userMock.loginUserInfo) //装换dto
			ImStorage.setImStorageSync(ImEnum.ImStorageEnum.loginUserInfo, loginUserInfo)

			/**
			 * 初始化会话列表
			 */
			store.dispatch(ImEnum.DataKeyEnum.CHAT_LIST, chatMock.data)
			/**
			 * 初始化 消息记录缓存
			 */
			let msgTemp = {
				"inChatUserId": 'group-100',
				"msg": msgMock.msgMap['group-100']
			}
			console.log(JSON.stringify(msgTemp))
			store.dispatch(ImEnum.DataKeyEnum.addMsgDataKey, msgTemp)

			/**
			 * 初始化通讯录
			 */
			let contacts = userMock.firendMap
			store.dispatch(ImEnum.DataKeyEnum.contactsDataKey, contacts)

			/**
			 * 初始化生活圈
			 */
			let sHQMsgList = sHQunMock.SHQMsgList
			store.dispatch(ImEnum.DataKeyEnum.initSHQMsgListDataKey, sHQMsgList)
			return true
		} else {
			/**
			 * 连接im服务
			 */
			if (!token || token === '') {
				let tokenCache = ImStorage.getImStorageSync(ImEnum.ImStorageEnum.loginNameAndPaw)
				if (tokenCache) {
					token = tokenCache
				} else {
					ImStorage.clearImStorageSync()
					ImStorage.setImStorageSync(ImEnum.ImStorageEnum.loginToken, token)
				}
			} else {
				ImStorage.clearImStorageSync()
				ImStorage.setImStorageSync(ImEnum.ImStorageEnum.loginToken, token)
			}
			if (BenderIM.connectSocket(token)) {
				return true
				console.log('成功啦---')
				// let msg = {
				//      "cmd":"17",
				//      "userId":"用户id(必填项)",
				//      "type":"2"
				// }
				// BenderIM.sendSocketMessage(msg)
			}

		}
		return false
	},
	/**
	 * 会话页相关api
	 */
	ChatApi: BenderChatApi,
	/**
	 * 获取当前登录人的信息
	 */
	sendMessage: function(msg) {
		// const j_im_send_req = {
		// 	chatType: 1,
		// 	cmd: 11,
		// 	content: JSON.stringify(msg),
		// 	createTime: new Date().getMilliseconds(),
		// 	from: msg.sender,
		// 	group_id: '100',
		// 	msgType: 0
		// }
		return BenderIM.sendSocketMessage(msg)
	},
	getLoginUserInfo: function() {
		return ImStorage.getImStorageSync(ImEnum.ImStorageEnum.loginUserInfo)
	},
	UserApi: BenderUserApi


}
