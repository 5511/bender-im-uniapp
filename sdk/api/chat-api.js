import ImStorage from '../utils/ImStorage.js'
import ImEnum from '../enums/index.js'

const BenderChatApi = {}


/**
 * 获取会话列表
 */
BenderChatApi.chatList = function(){
	let chatList = ImStorage.getImStorageSync(ImEnum.ImStorageEnum.chatKey)
	if(!chatList){
		chatList =[]
	}
	return chatList
}

export default BenderChatApi