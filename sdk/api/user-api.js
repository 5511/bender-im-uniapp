import ImStorage from '../utils/ImStorage.js'
import ImEnum from '../enums/index.js'

const BenderUserApi = {}


/**
 * 获取会话列表
 */
BenderUserApi.chatList = function() {
	let chatList = ImStorage.getImStorageSync(ImEnum.ImStorageEnum.chatKey)
	if (!chatList) {
		chatList = []
	}
	return chatList
}

/**
 * 根据accountNum  获取用户的基本信息 
 * @param {Object} accountNum 账户
 */
BenderUserApi.getUserInfByAccountNum = function(accountNum) {
	let userInfo = {}
	debugger
	if(accountNum&&accountNum === '1'){
		userInfo = {
						name: '测试数据',
						nickName: '测试数据',
						accountNum: '1043195893',
						location: '江西/赣州',
						phone: '15555555555',
						tag: '好朋友/男朋友/闺蜜',
						remak: '不要好奇，我没有故事',
						friendFlag: 0, //是否为好友 1是 0不是
						blackUser: 0, //是否加入黑名单 ，1是 0是
						shqPic: [
							{
								id: 1,
								src: '/static/images/product/1.jpg'
							},
							{
								id: 2,
								src: '/static/images/product/2.jpg'
							},
							{
								id: 3,
								src: '/static/images/product/3.jpg'
							},
							{
								id: 4,
								src: '/static/images/product/4.jpg'
							},
							{
								id: 5,
								src: '/static/images/product/5.jpg'
							}
						]
					}
	}else{
		//todo 请求到后台获取
	}
	return userInfo
}


export default BenderUserApi
