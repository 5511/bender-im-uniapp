# bender-im-uniapp
done：
    前端页面的开发
noDo：
    后端只修改了部分，没看，有时间再说
todo：
    后端对j-im的二次开发

欢迎大家start

#### 介绍


项目结构
   
```
- commom 页面的一些静态css资源 tabbar等
    -- static 公用css thorUI 的css 等
    -- 抽出来的tabbar
- components 组件 组件都是基于uniapp的easycom方式 引入
    -- im 自己抽的im组件
        --- im-addBtn 悬浮按钮
        --- imchatlist 会话列表组件
        --- imemoji 表情组件
        --- im-inChat-foot-input 消息发送组件
        --- im-media 语音视频播放组件
        --- im-msgBox 消息记录显示组件
        --- im-navBar
    --thorui thorui的组件 
- config 配置页面功能（暂时不用）
- pages 页面
    -- chat 会话窗口页面
    -- contacts 联系人页面
    -- discover 发现页面
    -- inChat 聊天页面
    -- me 我的 页面
- sdk im的sdk 打算且朝着做一个公用的 sdk 可以对接如何im 后台 也可以对接任何im页面
    -- api 
        --- chat-api.js 会话页面相关的 api
        --- JIM.js jim相关的imClient
    -- config imsdk相关配置
        --- index.js sdk主要配置 如配置是否使用假数据等
        --- transform-util 请求或回参的转化工具 此处用于实现sdk对接任何 im后端或 im页面
            ---- index.js
        --- enums im的一些枚举
        --- mock im的假数据
        --- store im的一些vuex 具体的查看下面的 readme
- static 静态资源
- APP.VUE
- main.js
- manifest.json
- pages.json
```

